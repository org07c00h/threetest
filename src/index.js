import * as THREE from 'three'

let camera, scene, renderer;
let mesh;
var meter = new FPSMeter({
    theme: 'colorful',
    heat: 1,
    graph:   1, // Whether to show history graph.
    history: 20 // How many history states to show in a graph.    
});

init();

animate();

function init() {

    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 0;

    scene = new THREE.Scene();

    const texture = new THREE.TextureLoader().load('assets/4.png');
    // const geometry = new THREE.BoxBufferGeometry(200, 200, 200);
    const geometry = new THREE.SphereGeometry(400, 8, 6);
    const material = new THREE.MeshBasicMaterial({
        map: texture,
        side: THREE.BackSide
    });

    mesh = new THREE.Mesh(geometry, material);
    scene.add(mesh);

    renderer = new THREE.WebGLRenderer({ antialias: false });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);
}

function animate() {
    requestAnimationFrame(animate);
    meter.tick();

    mesh.rotation.x += 0.005;
    mesh.rotation.y += 0.01;
    renderer.render(scene, camera);
}
